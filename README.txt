To launch the application! 

navigate to the built_application directory
Inside the directory will be two folder,
depending on your OS either enter the linux of windows directory

Inside will be an executable file called Game of Life, run this file
for the application to start. (works better on windows.)

LOCATION OF EXECUTABLE for Windows = GameofLife/built_application/windows-amd64/game_of_life.exe
LOCATION OF EXECUTABLE for Linux= GameofLife/built_application/linux-amd64/game_of_life



IF this doesn't work, the application can be built and run using the following
command on windows in the top level directory (GameofLife)

.\processing\processing-java --sketch=$PWD/src/game_of_life –run

this should run the application.



application can be controlled using these commands, click on the open application window and then press these keys to perform the
corresponding action (NOTE: some commands require the simulation to be paused, which can be accomplished by pressing space).

Key	Functionality

r	Restarts the game of life with new beginning config
c	Clears the board
space	Pauses simulation
q	Close the application
i	Zoom’s in 
d	Zoom’s out
o	Open’s file explorer to enter custom starting config (simulation MUST BE PAUSE)
Left Mouse Click	Pressing the left mouse on a dead cell will make it alive, and will make it dead if it is already alive. (simulation MUST BE PAUSE)


when Using the 'o' key to load in a custom config, custom configs can be found in the config directory, simply select the config and the simulation
should load in the info.







IF IT STILL DOES NOT WORK (LAST RESORT!!)
-------------------------------------------------------------------
if for whatever reason the application still refuses to run,
entering the processing directory, an application called processing can be found
run this application, and then load the sketchbook from the 
src/game_of_life directory. Then simply hit the play button.
------------------------------------------------------------------