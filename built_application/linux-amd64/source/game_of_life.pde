//Can be changed to allow greater chance of alive cells being spawned
//and can also change the colour of the alive and dead cells
float probAlive = 15;
color alive = color(0, 255, 255);
color dead = color(0, 0, 0);
//-------------------------------------------------------------------
int cellSize = 5;
int intervals = 100;
int prev_time = 0;
boolean paused = false;
int current_alive = 0;
int pop = 0;
int tick = 0;
int width_ = 1200;
int height_ = 600;
PFont f;

int[][] cells;
int[][] buffer;

/**
* Creates the inital setup enviroment for the application. 
* When the application is first run, the setup function is called by Processing
* While it takes no parameters, it prepares the enviroment 
* <p>
* This method always returns immediately, whether or not the 
* image exists. When this applet attempts to draw the image on
* the screen, the data will be loaded. The graphics primitives 
* that draw the image will incrementally paint on the screen. 
*
* @see         setup
*/
void setup()
{
  f = createFont("Arial",16,true);
  float state_prob = 0;
  int state_status = 0;
  size (1200, 600);
  //surface.setResizable(true);
  int width_cell = set_size(width_);
  int height_cell = set_size(height_);

  cells = new int[width_cell][height_cell];
  buffer = new int[width_cell][height_cell];
  stroke(35);

  for (int i = 0; i < width_cell; i++)
  {
    for (int x = 0; x < height_cell; x++)
    {
      state_prob = random(100);

      if (state_prob > probAlive)
      {
        state_status = 0;
      } else
      {
        pop++;
        state_status = 1;
      }
      cells[i][x] = state_status;
    }
  }
}

public int set_size(int heightOrWidth)
{
  int cell_result = heightOrWidth/cellSize;
  
  return cell_result;
}

void draw()
{
  int width_cell = set_size(width_);
  int height_cell = set_size(height_);

  for (int i = 0; i < width_cell; i++)
  {
    for (int x = 0; x < height_cell; x++)
    {
      if (cells[i][x] == 1)
      {
        fill(alive);
      } else
      {
        fill(dead);
      }
      rect(i*cellSize, x*cellSize, cellSize, cellSize);
    }
  }

  if (millis() - prev_time > intervals)
  {
    if (!paused)
    {
      time_tick();
      tick++;
      prev_time = millis();
      if(tick == 100)
      {
        tick = 0;
      }
    }
  }  
  textFont(f);
  fill(255,0,0);
  textAlign(RIGHT);
  text("Current Population: " + pop, width,20);
  text("Current Generation: " + tick, width,40);

  if(paused && mousePressed)
  {
    cellClicked();
  }
  else if(paused && !mousePressed)
  {
    for(int x = 0; x < width_/cellSize; x++)
    {
      for(int y = 0; y < height_/cellSize; y++)
      {
        buffer[x][y] = cells[x][y];
      }
    }
  }
}

void time_tick()
{
  int neighbours = 0;
  int width_cell = set_size(width_);
  int height_cell = set_size(height_);

  for (int i = 0; i < width_cell; i++)
  {
    for (int x = 0; x < height_cell; x++)
    {
      buffer[i][x] = cells[i][x];
    }
  }

  for (int i = 0; i < width_cell; i++)
  {
    for (int x = 0; x < height_cell; x++)
    {
      neighbours = 0;

      for (int i_1 = i-1; i_1 <= i+1; i_1++)
      {
        for (int x_1 = x-1; x_1 <= x+1; x_1++)
        {
          if (((i_1 >= 0) && (i_1 < width_cell)) && ((x_1 >= 0) && (x_1 < height_cell)))
          {
            if (!((i_1 == i) && (x_1 == x)))
            {
              if (buffer[i_1][x_1] == 1)
              {
                neighbours++;
              }
            }
          }
        }
      }
      if (buffer[i][x] == 1)
      {
        if (neighbours < 2 || neighbours > 3)
        {
          cells[i][x] = 0;
          pop--;
        }
      } else
      {
        if (neighbours == 3)
        {
          cells[i][x] = 1;
          pop++;
        }
      }
    }
  }
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());
    //path_to_csv = selection.getAbsolutePath();
    if (selection.getAbsolutePath().endsWith(".csv"))
    {
      //String[] pieces = split(line, ',');
      String [] lines = loadStrings(selection.getAbsolutePath());
      int col_length = 0;
      int row_length = 0;

      for (String line : lines)
      {
        String[] pieces = split(line, ',');
        row_length = pieces.length;
        col_length++;
      }
      int width_cell = set_size(width_);
      int height_cell = set_size(height_);
      int state_status = 0;

      cells = new int[width_cell][height_cell];
      buffer = new int[width_cell][height_cell];

      int y = 0;
      int x = 0;
      pop = 0;
      tick = 0;
      for (String line : lines)
      {
        String[] pieces = split(line, ',');
        for (String num : pieces)
        {
          int csv_value = Integer.parseInt(num);
          if (csv_value == 0)
          {
            state_status = 0;
          } else
          {
            pop++;
            state_status = 1;
          }
          cells[x][y] = state_status;
          x++;
        }
        x = 0;
        y++;
      }
    }
  }
}

void resetCells()
{
  cellSize = 5;
  pop = 0;
  tick = 0;

  for (int i = 0; i < width_/cellSize; i++)
  {
    for (int x = 0; x < height_/cellSize; x++)
    {
      float state = random(100);
      if (state > probAlive)
      {
        state = 0;
      } else
      {
        pop++;
        state = 1;
      }
      cells[i][x] = int(state);
    }
  }
}

void cellClicked()
{
  int width_cell = set_size(width_);
  int height_cell = set_size(height_);
  int mouse_cell_x = int(map(mouseX, 0, width, 0, width_cell));
  int mouse_cell_y = int(map(mouseY, 0, height, 0, height_cell));
  
  mouse_cell_x = constrain(mouse_cell_x, 0, (width_cell-1));
  mouse_cell_y = constrain(mouse_cell_y, 0, (height_cell-1));
  
  if(buffer[mouse_cell_x][mouse_cell_y] == 0)
  {
    cells[mouse_cell_x][mouse_cell_y] = 1;
    fill(alive);
  }
  else
  {
    cells[mouse_cell_x][mouse_cell_y] = 0;
    fill(dead);
  }
}


void keyPressed()
{
  if (key == 'r')
  {
    resetCells();
  }
  
  if(key == 'c')
  {
    for(int i = 0; i < set_size(width_); i++)
    {
      for(int x = 0; x < set_size(height_); x++)
      {
        cells[i][x] = 0;
        pop = 0;
        tick = 0;
      }
    }
  }

  if (key == ' ')
  {
    paused = !paused;
  }

  if (key == 'q')
  {
    exit();
  }

  if (key == 'i')
  {
    cellSize = cellSize + 1;
  }

  if (key == 'd' && cellSize > 5)
  {
    cellSize = cellSize - 1;
  }

  if (key == 'o' && paused == true)
  {
    selectInput("Select a file to process:", "fileSelected");
  }
}
